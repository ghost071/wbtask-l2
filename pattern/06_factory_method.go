package pattern

/*
	Реализовать паттерн «фабричный метод».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Factory_method_pattern

	Фабричный метод - порождающий паттерн проектирования, который решает проблему создания различных продуктов, без указания конкретных типов объектов.
*/

// Единый интерфейc
type Object interface {
	Method1() interface{}
	Method2() int
}

// Функция создает объект нужного типа в зависимости от переданного аргумента.
func New(ObjectType string) Object {
	switch ObjectType {
	case "O1":
		return NewObj1()
	case "O2":
		return NewObj2()
	default:
		return nil
	}
}

// Структры могут сильно отличаться друг от друга набором полей/методов, при этом реализуют интерфейс Object
type Object1 struct{}
type Object2 struct{}

func (o *Object1) Method1() interface{} {
	return 0
}

func (o *Object1) Method2() interface{} {
	return 0
}

func (o *Object2) Method1() int {
	return 0
}

func (o *Object2) Method2() int {
	return 0
}

func NewObj1() *Object1 {
	return &Object1{}
}

func NewObj2() *Object2 {
	return &Object2{}
}
