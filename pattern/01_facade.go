package pattern

import (
	"fmt"
	"strconv"
)

/*
	Реализовать паттерн «фасад».
Объяснить применимость паттерна, его плюсы и минусы,а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Facade_pattern

	Структурный паттерн. Предоставляет простой интерфейс к сложной системе.
	Проблема: для совершения какой-то операции требуется последовательно обратиться к нескольким сервисам.

	+Изолирует клиентов от сложной подсистемы
	-Рискует стать божественным объектом
*/

type Item struct {
	Name  string
	Price float64
}

type Shop struct {
	Name  string
	Items []*Item
}

func (s *Shop) Sell(user User, itemName string) error {
	card := user.Card
	bank := card.Bank
	balance, err := bank.CheckBalance(card.Number)
	if err != nil {
		return err
	}
	item, err := s.GetItem(itemName)
	if err != nil {
		return err
	}

	if balance < item.Price {
		return fmt.Errorf("Insufficient funds")
	}
	return nil
}

func (s *Shop) GetItem(name string) (*Item, error) {
	for _, i := range s.Items {
		if i.Name == name {
			return i, nil
		}
	}
	return nil, fmt.Errorf("Item with name %s not exist", name)
}

type Card struct {
	Name    string
	Balance float64
	Number  string
	Bank    *Bank
}

func (c *Card) CheckBalance(cardNumber string) (float64, error) {
	return c.Bank.CheckBalance(cardNumber)
}

type Bank struct {
	Name  string
	Cards map[string]Card
}

func (b *Bank) CheckBalance(cardNumber string) (float64, error) {

	valid := func(s string) bool {
		n, err := strconv.Atoi(cardNumber)
		if len(s) != 4 || err != nil {
			return false
		}
		return true
	}

	if !valid(cardNumber) {
		return 0, fmt.Errorf("Invalid card number %s", cardNumber)
	}

	if card, ok := b.Cards[cardNumber]; ok {
		return card.Balance, nil
	}
	return 0, fmt.Errorf("Card with num %s not exist", cardNumber)
}

type User struct {
	Name string
	Card *Card
}

func (u *User) CheckBalance() (float64, error) {
	return u.Card.CheckBalance(u.Card.Number)
}
