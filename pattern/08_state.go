package pattern

/*
	Реализовать паттерн «состояние».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/State_pattern

	Поведенческий паттерн. Позволяет объектам менять поведение в зависимоти от своего состояния.
	Состояния постоянно сменяют друг друга объект по разному реагирует на одни и те же события в разных состояниях

	Пример с часами.
	Часы имеют 3 кнопки:
	1. Изменить минуты (M)
	2. Изменить часы (H)
	3. Перейти в режим настройки будильника (Mode) // В этом режиме первые 2 кнопки изменяют время будильника

	Долгое нажатие на кнопку Mode активирует будильник если он выключен и наоборот
	Состояния:
	1. Часы
	2. Настройка будильника
	3. Звонок
	Звонок может иметь какую-то длительность и автоматически переходить в состояние 1
	Кнопки (Н) (M) в этом состоянии ничего не делают
	Кнопка (Mode) возвращает в состояние 1
*/

type state interface {
	clickH()
	clickM()
	clickMode()
	clickModeLong()
}

type minutes int
type hours int

func (m *minutes) tick() {
	if *m == 59 {
		*m = 0
		return
	}
	*m += 1
}

func (h *hours) tick() {
	if *h == 23 {
		*h = 0
		return
	}
	*h += 1
}

type clock struct {
	clockMinutes minutes
	clockHours   hours
	alarmMinutes minutes
	alarmHours   hours
	alarm        bool
	state        state
}

func (c *clock) clickH() {
	c.state.clickH(c)
}

func (c *clock) clickM() {
	c.state.clickM(c)
}

func (c *clock) clickMode() {
	c.state.clickMode(c)
}

func (c *clock) clickModeLong() {
	c.state.clickModeLong()
}

func (c *clock) checkBellTime() bool {
	if c.clockMinutes == c.alarmMinutes && c.clockHours == c.alarmHours {
		return true
	}
	return false
}

//================== State 1 ====================

type clockState struct {
	clock *clock
}

func (s *clockState) clickM() {
	s.clock.clockMinutes.tick()
	if s.clock.alarm && s.clock.isBellTime() {
		s.clock.state = bellState
	}
}

func (s *clockState) clickH() {
	s.clock.clockHours.tick()
	if s.clock.alarm && s.clock.isBellTime() {
		s.clock.state = bellState
	}
}

func (s *clockState) clickMode() {
	s.clock.state = alarmState
}

func (s *clockState) clickModeLong() {
	s.clock.alarm = !s.clock.alarm
}

//================== State 2 ====================

type alarmState struct {
	clock *clock
}

func (s *alarmState) clickM() {
	s.clock.alarmMinutes.tick()
	if s.clock.alarm && s.clock.isBellTime() {
		s.clock.state = bellState
	}
}

func (s *alarmState) clickH() {
	s.clock.alarmHours.tick()
	if s.clock.alarm && s.clock.isBellTime() {
		s.clock.state = bellState
	}
}

func (s *alarmState) clickMode() {
	s.clock.state = clockState
}

func (s *alarmState) clickModeLong() {
	s.clock.alarm = !s.clock.alarm
}

//================== State 3 ====================

type bellState struct {
	clock *clock
}

func (s *alarmState) clickM() {
}

func (s *alarmState) clickH() {
}

func (s *alarmState) clickMode() {
	s.clock.state = clockState
}

func (s *alarmState) clickModeLong() {
}
