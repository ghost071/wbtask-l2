package pattern

import "fmt"

/*
	Реализовать паттерн «цепочка вызовов».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern

	Поведенческий паттерн, позволяет передавать запросы последовательно по цепочке обработчиков. Каждый обработчик решает, обработать запрос самостоятельно или передать следующему обработчику.

	Проблема: требуется обработать клиентский запрос. Например в системе может быть определено множество ролей, тогда требуется идентифицировать клиента и предоставить соответствующий интерфейс. Со временем система может усложняться, могут добавляться новые роли, валидаторы. Код отвечающий за выбор обработчика будет постоянно раздуваться.

	Паттерн предлагает завернуть каждую проверку в отдельный объект и связать объекты в нужном порядке. Цепочка может быть любой длины запрос может быть обработан на любом этапе (в засивимости от его параметров).
	Обработчики могут быть реализованы как связный список или как дерево (текущий обработчик решает по какой ветке нанаправится запрос, в зависимости от результатов своей проверки)

	+Уменьшает зависимость между клиентом и обработчиками
	+Принцип единстевнной обязанности
	+Прицип открытости/закрытости

	-Запрос может остаться не обработанным

*/

type Service interface {
	Execute(*Data)
	SetNext(Service)
}

type Data struct {
	GetSource bool
	Update    bool
}

//====================== Service 1 ========================
type Device struct {
	Name string
	Next Service
}

func (d *Device) Execute(data *Data) {
	if data.GetSource {
		fmt.Printf("Data from device %s already get\n", d.Name)
		device.Next.Execute(data)
		return
	}
	fmt.Printf("Get data from device %s\n", d.Name)
	data.GetSource = true
	device.Next.Execute(data)
}

func (d *Device) SetNext(s Service) {
	d.Next = s
}

//======================== Service 2 ========================

type UpdateService struct {
	Name string
	Next Service
}

func (u *UpdateService) Execute(data *Data) {
	if data.UpdateSource {
		fmt.Printf("Data in service %s already update\n", u.Name)
		u.Next.Execute(data)
		return
	}
	fmt.Printf("Update data%s\n", d.Name)
	data.UpdateSource = true
	u.Next.Execute(data)
}

func (u *UpdateService) SetNext(s Service) {
	u.Next = s
}

//======================== Service 3 ========================

type DataService struct {
	Next Service
}

func (d *DataService) Execute(data *Data) {
	if data.UpdateSource {
		fmt.Println("Data save")
		return
	}
	fmt.Println("Data not update")
}

func (d *DataService) SetNext(s Service) {
	d.Next = s
}

//======================== Usage ========================

func main() {
	device := &Device{Name: "Device1"}
	updateSrv := &UpdateService{Name: "Update1"}
	saveSrv := &DataService{}
	device.SetNext(updateSrv)
	updateSrv.SetNext(saveSrv)

	data := &Data{}
	device.Execute(data)
}
