package pattern

import "fmt"

/*
	Реализовать паттерн «строитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Builder_pattern

	Строитель - порождающий паттерн проектирования.
	Проблема: необходима возможность инициализировать сложный объект (с множеством полей) в различных конфигурациях.
	Клиенту необходимо каждый раз передавать в конструктор огромный список параметров, даже если в большистве случаев нужна будет лишь малая часть этих параметров. При этом сам конструктор скорей всего будет огромной функцией с множеством ветвлений, тк необходимо предусмотреть всевозможные представления объекта.

	Целью паттерна является отделение постороения сложного объекта от его представления. Один и тот же процесс может порождать различные представления объекта.
	Паттерн предлагает создать отдельные объекты, отвечающие за инициализацию объекта (билдеры)

*/

type Builder interface {
	SetCore()
	SetMemory()
	SetMonitor()
	SetKeyboard()
	SetGraphicCard()
	GetComputer() Computer
}

type BuilderType string

func (bt BuilderType) GetBuilder() Builder {
	switch bt {
	case "Notebook":
		return &NotebookBuilder{}
	case "PC":
		return &PCBuilder{}
	default:
		return nil
	}
}

type Computer struct {
	Core        int
	Memory      int
	Monitor     int
	GraphicCard int
	Keyboard    int
}

type NotebookBuilder struct {
	Core        int
	Memory      int
	Monitor     int
	GraphicCard int
	Keyboard    int
}

func (nb *NotebookBuilder) SetCore() {
	nb.Core = 8
}

func (nb *NotebookBuilder) SetMemory() {
	nb.Memory = 16
}

func (nb *NotebookBuilder) SetKeyboard() {
	nb.Keyboard = 1
}

func (nb *NotebookBuilder) SetMonitor() {
	nb.Monitor = 1
}

func (nb *NotebookBuilder) SetGraphicCard() {
	nb.GraphicCard = 1
}

func (nb *NotebookBuilder) GetComputer() Computer {
	return Computer{
		Core:        nb.Core,
		Memory:      nb.Memory,
		Monitor:     nb.Monitor,
		Keyboard:    nb.Keyboard,
		GraphicCard: nb.GraphicCard,
	}
}

type PCBuilder struct {
	Core        int
	Memory      int
	GraphicCard int
	Monitor     int
	Keyboard    int
}

func (p *PCBuilder) SetCore() {
	p.Core = 32
}

func (p *PCBuilder) SetMemory() {
	p.Memory = 128
}

func (p *PCBuilder) SetGraphicCard() {
	p.GraphicCard = 1
}

func (p *PCBuilder) SetMonitor() {
	p.Monitor = 0
}

func (p *PCBuilder) SetKeyboard() {
	p.Keyboard = 0
}

func (p *PCBuilder) GetComputer() Computer {
	return Computer{
		Core:        p.Core,
		Memory:      p.Memory,
		Keyboard:    p.Keyboard,
		Monitor:     p.Monitor,
		GraphicCard: p.GraphicCard,
	}
}

type Factory struct {
	Builder Builder
}

func NewFactory(b Builder) *Factory {
	return &Factory{Builder: b}
}

func (f *Factory) SetBuilder(b Builder) {
	f.Builder = b
}

func (f *Factory) CreateComputer() Computer {
	f.Builder.SetCore()
	f.Builder.SetMemory()
	f.Builder.SetKeyboard()
	f.Builder.SetMonitor()
	f.Builder.SetGraphicCard()
	return f.Builder.GetComputer()
}

//========= Usage =================

const (
	Notebook BuilderType = "Notebook"
	PC       BuilderType = "PC"
)

func main() {
	builder1 := Notebook.GetBuilder()
	builder2 := PC.GetBuilder()
	factory := NewFactory(builder1)
	computer1 := factory.CreateComputer()

	factory.SetBuilder(builder2)
	computer2 := factory.CreateComputer()
	fmt.Printf("%#v\n%#v\n", computer1, computer2)
}
