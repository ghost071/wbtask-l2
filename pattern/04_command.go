package pattern

/*
	Реализовать паттерн «комманда».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Command_pattern

	Поведенческий паттерн позволяющий заворачивать запросы в отдельные объекты.
	Отправитель запроса ничего не знает об исполнителе, отправителю достаточно знать интерфейс объекта команды.

	+убирает прямую зависимость между объектом отправителем и исполнителем
	+позволяет собирать сложные команды из простых
	+реализует принцип открытости закрытости
	+позволяет откладывать выполнение операций

	-усложняет код программы из-за введения множества дополнительных объектов
*/

type button struct {
	command command
}

func (b *button) press() {
	b.command.execute()
}

type command interface {
	execute()
}

type onCommand struct {
	device device
}

func (c *onCommand) execute() {
	c.device.on()
}

type offCommand struct {
	device device
}

func execute() {
	c.device.off()
}

type device interface {
	on()
	off()
}

type tv struct {
	isRunning bool
}

func (t *tv) on() {
	t.isRunning = true
	fmt.Println("Turning tv on")
}

func (t *tv) off() {
	t.isRunning = false
	fmt.Pritnln("Turning tv off")
}

func main() {
	tv := &tv{}
	on := &onCommand{
		device: tv,
	}

	off := &offCommand{
		device: tv,
	}

	onButton := &button{
		command: on,
	}

	onButton.press()

	offButton := &button{
		command: off,
	}
	offButton.press()
}
