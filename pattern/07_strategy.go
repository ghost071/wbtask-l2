package pattern

import "fmt"

/*
	Реализовать паттерн «стратегия».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Strategy_pattern

	Поведенческий паттерн. Определяет семейство схожих алгоритмов и помещает каждый из них в собственную структуру, после чего алгоритмы можно взаимозаменять прямо во время исполнения программы

	+Легкая смена алгоритмов
	+Изолирует алгоритмы от объектов
	+Принцип открытости закрытости

	-Усложняет код дополнительными объектами
*/

type sortStrategy interface {
	sort(data *data)
}

type quickSort struct{}

func (s *quickSort) sort(data *data) {
	arr := data.items
	qsort(&arr, 0, len(arr)-1)
}

type mergeSort struct{}

func (s *mergeSort) sort(data *data) {
	arr := data.items
	data.items = msort(arr)
}

type data struct {
	items    []int
	strategy sortStrategy
}

func (d *data) setStrategy(s sortStrategy) {
	d.strategy = s
}

func (d *data) sort() {
	d.strategy.sort(d)
}

func main() {
	arr := []int{52, 23, 2, 8, 91, 4}
	arr2 := []int{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
	data := &data{
		items: arr,
	}

	qsort := &quickSort{}
	msort := &mergeSort{}

	data.setStrategy(qsort)
	data.sort()
	fmt.Println(data.items)

	data.items = arr2
	data.setStrategy(msort)
	data.sort()
	fmt.Println(data.items)
}

//===================================================================

func qsort(arr *[]int, lIdx, rIdx int) {
	if len(*arr) < 2 || !(lIdx < rIdx) {
		return
	}

	idx := (rIdx-lIdx)/2 + lIdx
	median := (*arr)[idx]

	i := lIdx
	j := rIdx

	for i <= j {
		if (*arr)[i] <= median {
			i++
		} else if (*arr)[j] >= median {
			j--

		} else {
			(*arr)[i], (*arr)[j] = (*arr)[j], (*arr)[i]
			i++
			j--
		}
	}

	if i > idx {
		(*arr)[idx], (*arr)[j] = (*arr)[j], (*arr)[idx]
		qsort(arr, lIdx, j-1)
		qsort(arr, j+1, rIdx)

	} else {
		(*arr)[idx], (*arr)[i] = (*arr)[i], (*arr)[idx]
		qsort(arr, lIdx, i-1)
		qsort(arr, i+1, rIdx)
	}
}

func msort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}
	mid := len(arr) / 2
	left := msort(arr[:mid])
	right := msort(arr[mid:])
	var newArray []int
	l := 0
	r := 0
	for l < len(left) && r < len(right) {
		if left[l] > right[r] {
			newArray = append(newArray, right[r])
			r++
		} else {
			newArray = append(newArray, left[l])
			l++
		}
	}
	if l < len(left) {
		newArray = append(newArray, left[l:]...)
	} else if r < len(right) {
		newArray = append(newArray, right[r:]...)
	}
	return newArray
}
