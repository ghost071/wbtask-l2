package pattern

import (
	"fmt"
	"math"
)

/*
	Реализовать паттерн «посетитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Visitor_pattern

	Поведенческий паттерн
	Проблема: по каким-то причинам нельзя изменять существующие объекты, но при этом необходимо добавить им новый функционал.
	Решение: можно создавать новые объекты посетителей и передавать объекты в методы посетителя.
	Необходимо описать интерфейс посетителя и в заложить в существующие объекты метод, который будет принимать посетителя и вызывать у него соответствующий метод. Это подразумевает изменение существующих объектов, но достаточно добавить лишь один метод и лишь однажды. Затем можно передавать в этот метод разных посетителей, которые могут выполнять свою логику.
	+Упрощает добавление новых операций
	+Объединяет родственные операции в одном месте

	-Не оправдан, если часто меняется структура объектов
*/
type shape interface {
	accept(visitor) float64
}

type square struct {
	x float64
}

func (s *square) accept(v visitor) float64 {
	return v.visitForSquare(s)
}

type circle struct {
	radius float64
}

func (c *circle) accept(v visitor) float64 {
	return v.visitForCircle(c)
}

type rectangle struct {
	a     float64
	b     float64
	angle float64
}

func (r *rectangle) accept(v visitor) float64 {
	return v.visitForRectangle(r)
}

//========== Visitor interface ===========
type visitor interface {
	visitForSquare(*square) float64
	visitForCircle(*circle) float64
	visitForRectangle(*rectangle) float64
}

//========== Visitors ==============

type perimeterCalc struct {
}

func (p *perimeterCalc) visitForSquare(s *square) float64 {
	return s.x * 4
}

func (p *perimeterCalc) visitForCircle(c *circle) float64 {
	return math.Pi * 2 * c.radius
}

func (p *perimeterCalc) visitForRectangle(r *rectangle) float64 {
	rad := math.Pi / 180 * r.angle
	c := math.Sqrt(r.a*r.a + r.b*r.b - 2*r.a*r.b*math.Cos(rad))
	return r.a + r.b + c
}

type areaCalc struct {
}

func (a *areaCalc) visitForSquare(s *square) float64 {
	return s.x * s.x
}

func (a *areaCalc) visitForCircle(c *circle) float64 {
	return math.Pi * c.radius * c.radius
}

func (a *areaCalc) visitForRectangle(r *rectangle) float64 {
	rad := math.Pi / 180 * r.angle
	return r.a * r.b * math.Sin(rad) / 2
}

//========== Usage ===========

func main() {
	square := &square{3}
	circle := &circle{10}
	rectangle := &rectangle{3, 4, 90}

	areaCalc := &areaCalc{}
	fmt.Printf("area of square %#v = %.2f\n", square, square.accept(areaCalc))
	fmt.Printf("area of circle %#v = %.2f\n", circle, circle.accept(areaCalc))
	fmt.Printf("area of rectangle %#v = %.2f\n", rectangle, rectangle.accept(areaCalc))

	prmCalc := &perimeterCalc{}
	fmt.Printf("perimeter of square %#v = %.2f\n", square, square.accept(prmCalc))
	fmt.Printf("perimeter of circle %#v = %.2f\n", circle, circle.accept(prmCalc))
	fmt.Printf("perimeter of rectangle %#v = %.2f\n", rectangle, rectangle.accept(prmCalc))
}
