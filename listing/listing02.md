Что выведет программа? Объяснить вывод программы. Объяснить как работают defer’ы и их порядок вызовов.

```go
package main

import (
	"fmt"
)


func test() (x int) {
	defer func() {
		x++
	}()
	x = 1
	return
}


func anotherTest() int {
	var x int
	defer func() {
		x++
	}()
	x = 1
	return x
}


func main() {
	fmt.Println(test())
	fmt.Println(anotherTest())
}
```
Ответ:
```
test: 2
anotherTest: 1
...
Во втором случае значение переменной копируется для возврата, при это сам defer отрабатывает корректно в этом можно убедиться следущим образом:
```

```go
func anotherTest() int {
	var x int

	defer func() {		// Добавленный defer
		fmt.Printf("from added func %d\n", x)
	}() 							

	defer func() {
		x++
	}()
	x = 1
	return x
}
```
```
Если заменить функцию anotherTest(), то будет выведено:
2
from added func 2
1
Деферы отработают в порядке LIFO, как видно из примера. Следовательно х++ отрабатывает корректно.
defer - отложенный вызов функции, выполняется перед выходом из внешней функции. 
```
