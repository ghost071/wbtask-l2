Что выведет программа? Объяснить вывод программы.

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func asChan(vs ...int) <-chan int {
	c := make(chan int)

	go func() {
		for _, v := range vs {
			c <- v
			time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		}

		close(c)
	}()
	return c
}

func merge(a, b <-chan int) <-chan int {
	c := make(chan int)
	go func() {
		for {
			select {
			case v := <-a:
				c <- v
			case v := <-b:
				c <- v
			}
		}
	}()
	return c
}

func main() {

	a := asChan(1, 3, 5, 7)
	b := asChan(2, 4 ,6, 8)
	c := merge(a, b )
	for v := range c {
		fmt.Println(v)
	}
}
```

Ответ: будут выведены все числа (1-8) затем будут бесконечно выводиться нули

Чтение из закрытого канала возвращает zeroValue для своего типа, в данном случае 0. Таким образом в канал с постоянно будут записываться новые значения (нули).

Как фиксить? Чтение из канала на самом деле возвращает 2 значения. Второе значение булево сообщает состояние канала открыт/закрыт. Можно сохранять информацию о том, что канал был закрыт и проверять в горутине внутри мерж каждую итерацию остались ли незакрытые каналы. Когда все каналы закрыты, читать больше не нужно, можно закрывать мерж канал и убивать горутину.

```go
package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"
)

func asChan(vs ...int) <-chan int {
	c := make(chan int)

	go func() {
		for _, v := range vs {
			c <- v
			time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		}

		close(c)
	}()
	return c
}

func merge(a, b <-chan int) <-chan int {
	c := make(chan int)

	sig := make(chan struct{})
	ctx, cancel := context.WithCancel(context.Background())
	go handler(cancel, sig)

	go func() {
		for {
			select {
			case <-ctx.Done():
				close(c)
				return
			default:
			}

			select {

			case v, ok := <-a:
				if !ok {
					sig <- struct{}{}
					continue
				}
				c <- v
			case v, ok := <-b:
				if !ok {
					sig <- struct{}{}
					continue
				}
				c <- v
			}
		}
	}()
	return c
}

func handler(cancel context.CancelFunc, sig <-chan struct{}) {
	<-sig
	<-sig
	cancel()
	return
}

func main() {

	a := asChan(1, 3, 5, 7)
	b := asChan(2, 4, 6, 8)
	c := merge(a, b)
	for v := range c {
		fmt.Println(v)
	}
}
...

```
