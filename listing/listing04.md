Что выведет программа? Объяснить вывод программы.

```go
package main

func main() {
	ch := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
	}()

	for n := range ch {
		println(n)
	}
}
```

Ответ: 1, 2, ..., 9 deadlock
Чтение из канала блокирующая операция, а до тех пор пока канал открыт цикл не остановится.
Избежать дэдлока можно закрыв канал или явно прервав цикл. (в случае с каналами  range не знает сколько итераций понадобится)

```go
func noDeadLock1() {
	ch := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
		close(ch)	 // Как только прекратится запись в канал, канал закрывается
	}()

	for n := range ch {
		println(n)
	}
}
...

func noDeadLock2() {
	ch := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
	}()

	for i := 0; i < 10; i++ {
		n := <-ch
		println(n)
	}
}
```
