Что выведет программа? Объяснить вывод программы.

```go
package main

type customError struct {
	msg string
}

func (e *customError) Error() string {
	return e.msg
}

func test() *customError {
	{
		// do something
	}
	return nil
}

func main() {
	var err error
	err = test()
	if err != nil {
		println("error")
		return
	}
	println("ok")
}
```

Ответ: error
Тот же случай, что и в листинге 3 - типизированная ошибка обернутая в интерфейс сравнивается с nil

```go
fmt.Printf("%#v != %#v\n", err, nil)  // nil != (*main.customError)(nil)
...

```
