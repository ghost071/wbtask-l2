package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/spf13/pflag"
	"golang.org/x/net/html"
)

type wgStat struct {
	links map[string]struct{}
	lim   int
	host  string
}

func (w *wgStat) ContainsLink(l string) bool {
	if _, ok := w.links[l]; ok {
		return true
	}
	return false
}

func (w *wgStat) IsMaxDepth(depth int) bool {
	return depth >= w.lim
}

func newWg() *wgStat {
	return &wgStat{
		links: make(map[string]struct{}),
	}
}

func main() {
	wgS := newWg()

	pflag.IntVarP(&wgS.lim, "limit", "l", 1, "max depth")
	pflag.Parse()

	link := pflag.Arg(0)
	url, err := url.Parse(link)
	if err != nil {
		log.Fatal(err)
	}

	wgS.host = url.Host

	wget(link, wgS, 0)
}

func wget(addr string, wgS *wgStat, depth int) {
	if wgS.ContainsLink(addr) || wgS.IsMaxDepth(depth) {
		return
	}

	wgS.links[addr] = struct{}{}
	resp, err := http.Get(addr)

	if err != nil {
		log.Println(err)
		return
	}

	defer resp.Body.Close()
	log.Println(resp.Request.URL, "|", resp.Status)

	buff := bytes.NewBuffer([]byte{})
	if _, err = io.Copy(buff, resp.Body); err != nil {
		log.Println(err)
	}
	str := buff.String()

	fpath := wgS.host + resp.Request.URL.Path
	fname := "/index.html"

	node, err := html.Parse(buff)

	if err != nil {
		fmt.Println(err)
		return
	}

	if err = saveFile(fpath, fname, bytes.NewBuffer([]byte(str))); err != nil {
		log.Println(err)
	}

	walkTags(node, wgS, depth)
}

func walkTags(n *html.Node, wgS *wgStat, depth int) {
	cond1 := n.Type == html.ElementNode && n.Data == "a"
	cond2 := n.Type == html.ElementNode && n.Data == "img"

	switch {
	case cond1:
		for _, a := range n.Attr {
			if a.Key == "href" {
				link, err := url.Parse(a.Val)

				if err != nil {
					log.Println(err)
					continue
				}

				if l, err := checkLink(wgS.host, link); err == nil {
					wget(l, wgS, depth+1)
				}
			}
		}

	case cond2:
		for _, a := range n.Attr {
			if a.Key == "src" {
				link, err := url.Parse(a.Val)
				if err != nil {
					continue
				}

				if l, err := checkLink(wgS.host, link); err == nil {
					resp, err := http.Get(l)
					if err != nil {
						log.Println(err)
						continue
					}

					p := strings.Split(resp.Request.URL.Path, "/")
					fpath := wgS.host + strings.Join(p[:len(p)-1], "")
					fname := p[len(p)-1]
					if err = saveFile(fpath, fname, resp.Body); err != nil {
						log.Println(err)
					}
				}
			}
		}
	}
	for newN := n.FirstChild; newN != nil; newN = newN.NextSibling {
		walkTags(newN, wgS, depth)
	}
}

func saveFile(fpath, fname string, data io.Reader) error {
	if err := os.MkdirAll(fpath, 0755); err != nil {
		return err
	}

	f, err := os.Create(fpath + fname)
	if err != nil {
		return err
	}

	defer f.Close()
	if _, err := io.Copy(f, data); err != nil {
		return err
	}
	return nil
}

func checkLink(host string, link *url.URL) (string, error) {
	scheme := "https://"
	switch {
	case link.Host == "":
		return scheme + host + link.String(), nil
	case link.Host == host:
		if link.Scheme != "" {
			return link.String(), nil
		}
		return scheme + link.String(), nil
	default:
		return "", fmt.Errorf("external link")
	}
}
