package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

/*
=== Утилита grep ===

Реализовать утилиту фильтрации (man grep)

Поддержать флаги:
-A - "after" печатать +N строк после совпадения
-B - "before" печатать +N строк до совпадения
-C - "context" (A+B) печатать ±N строк вокруг совпадения
-c - "count" (количество строк)
-i - "ignore-case" (игнорировать регистр)
-v - "invert" (вместо совпадения, исключать)
-F - "fixed", точное совпадение со строкой, не паттерн
-n - "line num", печатать номер строки

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.

==== Описание ====
@ Программа принимает:
	- Вышеперечисленные флаги
	- Обязательный аргумент (паттерн) по которому будет выполнен поиск
	- Необязательный аргумент -- имя файла (по умолчанию stdin)

Поиск происходит по регулярному выражению
Если был задан флаг -F все квантификаторы экранируются

@ Примеры запуска:
	> go run task.go -n -A 1 -i -F ".txt" filename
	> cat filename | go run task.go -n -v "<\go"

==================
*/

const (
	RED   = "\033[01;03;38;05;168m"
	WHITE = "\033[0m"
)

type Options struct {
	After   int
	Before  int
	Context int
	Count   bool
	Lower   bool
	Invert  bool
	Fixed   bool
	LineNum bool
}

func main() {
	opt := &Options{}
	flag.IntVar(&opt.After, "A", 0, "show N strings after match")
	flag.IntVar(&opt.Before, "B", 0, "show N strings before match")
	flag.IntVar(&opt.Context, "C", 0, "show ±N strings around match")
	flag.BoolVar(&opt.Count, "c", false, "count matches")
	flag.BoolVar(&opt.Lower, "i", false, "ignore register")
	flag.BoolVar(&opt.Invert, "v", false, "exceptions instead match")
	flag.BoolVar(&opt.Fixed, "F", false, "exact match string, no pattern")
	flag.BoolVar(&opt.LineNum, "n", false, "print line number")

	flag.Parse()

	var filename, pattern string

	switch flag.NArg() {
	case 0:
		log.Fatal("Error")
	case 1:
		pattern = flag.Arg(0)
	case 2:
		pattern = flag.Arg(0)
		filename = flag.Arg(1)
	}

	var data []string

	switch filename {
	case "":
		if err := readFile(os.Stdin, &data); err != nil {
			log.Fatal(err)
		}
	default:
		f, err := os.Open(filename)

		if err != nil {
			log.Fatal(err)
		}

		if err = readFile(f, &data); err != nil {
			log.Fatal(err)
		}
	}
	grep(&data, pattern, opt)
}

func readFile(f io.Reader, buffer *[]string) error {
	r := bufio.NewReader(f)
	for {
		str, err := r.ReadString('\n')
		strings.Trim(str, "\n")
		if err == io.EOF {
			break
		}

		if err != nil {
			return err
		}
		(*buffer) = append((*buffer), str)
	}
	return nil
}

func grep(data *[]string, pattern string, opt *Options) {

	opt.Before = int(math.Max(float64(opt.Before), float64(opt.Context)))
	opt.After = int(math.Max(float64(opt.After), float64(opt.Context)))

	if opt.Lower {
		pattern = strings.ToLower(pattern)
		for i := range *data {
			(*data)[i] = strings.ToLower((*data)[i])
		}
	}

	filtered := filter(data, pattern, opt)

	if opt.Count {
		fmt.Println(len(filtered))
		return
	}
	err := write(os.Stdout, filtered)
	if err != nil {
		log.Fatal(err)
	}
}

func filter(data *[]string, pattern string, opt *Options) []string {
	res := make([]string, 0)

	re := func(s string, opt bool) *regexp.Regexp {
		if opt {
			return regexp.MustCompile(regexp.QuoteMeta(s))
		}

		return regexp.MustCompile(s)
	}(pattern, opt.Fixed)

	addContext := func(j, k int) string {
		newStr := ""
		if k-j > 1 {
			newStr = "---\n"
		}

		k = int(math.Min(float64(k), float64(len(*data))))

		for i := j; i < k; i++ {
			curr := (*data)[i]

			if re.MatchString(curr) {
				idx := re.FindAllStringIndex(curr, -1)
				curr = modify(curr, idx)
				newStr += addLineNum(curr, i+1, opt.LineNum)
				continue
			}
			newStr += addLineNum(curr, i+1, opt.LineNum)
		}
		return newStr
	}

	for i, s := range *data {
		if opt.Invert && !re.MatchString(s) {
			newStr := addContext(i-opt.Before, i+opt.After+1)
			res = append(res, newStr)
		}
		if !opt.Invert && re.MatchString(s) {
			newStr := addContext(i-opt.Before, i+opt.After+1)
			res = append(res, newStr)
		}
	}
	return res
}

func addLineNum(s string, n int, b bool) string {
	if b {
		k := strconv.Itoa(n)
		return k + ":" + s
	}
	return s
}

func modify(s string, idx [][]int) string {
	newStr := ""
	last := 0
	for _, i := range idx {
		newStr += s[last:i[0]] + RED + s[i[0]:i[1]] + WHITE
		last = i[1]
	}
	newStr += s[last:len(s)]
	return newStr
}

func write(f *os.File, data []string) error {
	for _, str := range data {

		_, err := io.WriteString(f, str)
		if err != nil {
			return err
		}
	}
	return nil
}
