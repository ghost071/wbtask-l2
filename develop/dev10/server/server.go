package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"strings"

	"github.com/spf13/pflag"
)

type connParam struct {
	protocol string
	host     string
	port     string
}

func (c *connParam) getAddr() string {
	return fmt.Sprintf("%s:%s", c.host, c.port)
}

func handleConnection(c net.Conn) {
	for {
		msg, err := bufio.NewReader(c).ReadString('\n')
		if err != nil && err != io.EOF {
			log.Println(err)
			return
		}
		temp := strings.TrimSpace(string(msg))
		temp = fmt.Sprintf("\"%s\"\n", temp)
		_, err = c.Write([]byte(temp))

		if err != nil {
			log.Println(err)
		}
	}
	c.Close()
}

func main() {
	cPar := &connParam{}

	pflag.StringVarP(&cPar.protocol, "proto", "P", "tcp", "tcp/udp")
	pflag.StringVarP(&cPar.host, "host", "h", "localhost", "hostname or hostIP")
	pflag.StringVarP(&cPar.port, "port", "p", "8000", "port num")

	pflag.Parse()

	l, err := net.Listen(cPar.protocol, cPar.getAddr())
	if err != nil {
		log.Println(err)
	}
	defer l.Close()
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go handleConnection(conn)
	}
}
