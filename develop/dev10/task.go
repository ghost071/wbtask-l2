package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/pflag"
)

/*
=== Утилита telnet ===

Реализовать примитивный telnet клиент:
Примеры вызовов:
go-telnet --timeout=10s host port go-telnet mysite.ru 8080 go-telnet --timeout=3s 1.1.1.1 123

Программа должна подключаться к указанному хосту (ip или доменное имя) и порту по протоколу TCP.
После подключения STDIN программы должен записываться в сокет, а данные полученные и сокета должны выводиться в STDOUT
Опционально в программу можно передать таймаут на подключение к серверу (через аргумент --timeout, по умолчанию 10s).

При нажатии Ctrl+D программа должна закрывать сокет и завершаться. Если сокет закрывается со стороны сервера, программа должна также завершаться.
При подключении к несуществующему сервер, программа должна завершаться через timeout.
*/

func main() {
	timeout := 10 * time.Second

	pflag.DurationVarP(&timeout, "timeout", "t", timeout, "use -t or --timeout to set timeout conn")
	pflag.Parse()

	host := pflag.Arg(0)
	port := pflag.Arg(1)
	if err := telnet(timeout, host, port); err != nil {
		fmt.Println(err)
	}
}

func telnet(timeout time.Duration, host, port string) error {

	ctx, cancel := context.WithCancel(context.Background())
	go sigHandler(cancel)

	subCtx, subCancel := context.WithTimeout(ctx, timeout)
	defer subCancel()

	addr := fmt.Sprintf("%s:%s", host, port)

	dialer := &net.Dialer{Timeout: timeout}
	conn, err := dialer.DialContext(subCtx, "tcp", addr)

	if err != nil {
		return err
	}

	defer func() {
		if err == nil {
			err = fmt.Errorf("%v", conn.Close())
			return
		}
		err = fmt.Errorf("%v\n%v", err, conn.Close())
	}()

	go readFromServer(ctx, cancel, conn)
	go readStdin(ctx, cancel, conn)
	<-ctx.Done()

	return err
}

func readFromServer(ctx context.Context, cancel context.CancelFunc, conn net.Conn) {
	r := bufio.NewReader(conn)
	for {
		select {
		case <-ctx.Done():
			return
		default:

			line, err := r.ReadString('\n')
			if err == io.EOF {
				cancel()
				return
			}

			if err != nil {
				fmt.Println(err)
				break
			}

			_, err = io.WriteString(os.Stdout, line)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

func readStdin(ctx context.Context, cancel context.CancelFunc, conn net.Conn) {
	r := bufio.NewReader(os.Stdin)
	for {
		select {
		case <-ctx.Done():
			return
		default:
			line, err := r.ReadString('\n')

			if err == io.EOF {
				cancel()
				return
			}

			if err != nil {
				fmt.Println(err)
				break
			}
			_, err = conn.Write([]byte(line))

			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

func sigHandler(cancel context.CancelFunc) {
	sig := make(chan os.Signal, 1)
	signal.Notify(
		sig,
		syscall.SIGHUP,
		syscall.SIGQUIT,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGPIPE,
	)

	<-sig
	cancel()
}
