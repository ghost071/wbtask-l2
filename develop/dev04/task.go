package main

import (
	"errors"
	"fmt"
	"sort"
	"strings"
)

/*
=== Поиск анаграмм по словарю ===

Напишите функцию поиска всех множеств анаграмм по словарю.
Например:
'пятак', 'пятка' и 'тяпка' - принадлежат одному множеству,
'листок', 'слиток' и 'столик' - другому.

Входные данные для функции: ссылка на массив - каждый элемент которого - слово на русском языке в кодировке utf8.
Выходные данные: Ссылка на мапу множеств анаграмм.
Ключ - первое встретившееся в словаре слово из множества
Значение - ссылка на массив, каждый элемент которого, слово из множества. Массив должен быть отсортирован по возрастанию.
Множества из одного элемента не должны попасть в результат.
Все слова должны быть приведены к нижнему регистру.
В результате каждое слово должно встречаться только один раз.

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {
	sl := []string{"листок", "слиток", "столик", "пятак", "пятка", "тяпка", "актяп", "нос", "сон", "резина"}
	fmt.Println(*buildAnagramSets(&sl))
}

type Set struct {
	vals map[string]struct{}
}

func (s Set) Add(w string) {
	s.vals[w] = struct{}{}
}

func buildAnagramSets(words *[]string) *map[string][]string {
	sets := make(map[string]*Set)
LOOP:
	for _, word := range *words {
		w := strings.ToLower(word)
		for k, set := range sets {
			if isBelong(k, w) {
				set.Add(w)
				continue LOOP
			}
		}
		set := &Set{vals: make(map[string]struct{})}
		set.Add(w)
		sets[w] = set
	}

	//fmt.Println(sets)

	ans := make(map[string][]string)

	for k, v := range sets {
		sl, err := toSlice(v)
		if err != nil {
			continue
		}
		ans[k] = sl
	}
	return &ans
}

func isBelong(set, word string) bool {
	w1 := strings.Split(set, "")
	w2 := strings.Split(word, "")

	sort.Strings(w1)
	sort.Strings(w2)

	if strings.Join(w1, "") == strings.Join(w2, "") {
		return true
	}

	return false
}

func toSlice(s *Set) ([]string, error) {
	ans := make([]string, 0)
	for k, _ := range s.vals {
		ans = append(ans, k)
	}
	if len(ans) == 1 {
		return ans, errors.New("set of one element")
	}

	sort.Strings(ans)
	return ans, nil
}
