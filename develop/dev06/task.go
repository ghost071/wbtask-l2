package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

/*
=== Утилита cut ===

Принимает STDIN, разбивает по разделителю (TAB) на колонки, выводит запрошенные

Поддержать флаги:
-f - "fields" - выбрать поля (колонки)
-d - "delimiter" - использовать другой разделитель
-s - "separated" - только строки с разделителем

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type colRange struct {
	start int
	end   int
}

func (c *colRange) String() string {
	return ""
}

func (c *colRange) Set(s string) error {
	vals := strings.Split(s, "-")
	if len(vals) > 2 {
		return errors.New("Wrong flag value")
	}

	if len(vals) == 2 {
		start, err1 := strconv.Atoi(vals[0])
		end, err2 := strconv.Atoi(vals[1])

		if err1 != nil || err2 != nil {
			err := fmt.Errorf("%v\n%v\n", err1, err2)
			return err
		}
		c.start = start
		c.end = end
		return nil
	}

	start, err := strconv.Atoi(vals[0])
	if err != nil {
		return err
	}

	c.start = start
	c.end = 0
	return nil
}

func readFile(f io.Reader, buffer *[]string) error {
	r := bufio.NewReader(f)
	for {
		str, err := r.ReadString('\n')
		strings.Trim(str, "\n")
		if err == io.EOF {
			break
		}

		if err != nil {
			return err
		}
		(*buffer) = append((*buffer), str)
	}
	return nil
}

func cut() error {
	rng := &colRange{}

	flagF := flag.String("f", "0-0", "")
	flagD := flag.String("d", " ", "delimeter")
	flagS := flag.Bool("s", false, "only strings whith delimeter")

	flag.Parse()

	fs := flag.NewFlagSet("", flag.ExitOnError)
	fs.Var(rng, "rng", "a")
	fs.Parse([]string{"-rng", *flagF})

	data := make([]string, 0)
	filename := flag.Arg(0)
	delim := *flagD
	separated := *flagS

	switch filename {
	case "":
		if err := readFile(os.Stdin, &data); err != nil {
			return err
		}
	default:
		f, err := os.Open(filename)

		if err != nil {
			return err
		}

		if err = readFile(f, &data); err != nil {
			return err
		}
	}

	out := os.Stdout

	for _, s := range data {
		sl := strings.Split(s, delim)
		err := write(out, sl, rng, separated, delim)

		if err != nil {
			return err
		}
	}
	return nil
}

func write(out *os.File, str []string, rng *colRange, flag bool, delim string) error {
	if len(str) == 1 && flag {
		return nil
	}

	if rng.start > rng.end {
		s := strings.Join(str[rng.start:], delim)
		_, err := io.WriteString(out, s)
		if err != nil {
			return err
		}
		return nil
	}

	if rng.end > len(str) {
		rng.end = len(str) - 1
	}

	s := strings.Join(str[rng.start:rng.end+1], delim)
	_, err := io.WriteString(out, s)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	if err := cut(); err != nil {
		log.Fatal(err)
	}
}
