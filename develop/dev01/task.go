package main

import (
	"fmt"
	"github.com/beevik/ntp"
	"os"
	"time"
)

/*
=== Базовая задача ===

Создать программу печатающую точное время с использованием NTP библиотеки.Инициализировать как go module.
Использовать библиотеку https://github.com/beevik/ntp.
Написать программу печатающую текущее время / точное время с использованием этой библиотеки.

Программа должна быть оформлена с использованием как go module.
Программа должна корректно обрабатывать ошибки библиотеки: распечатывать их в STDERR и возвращать ненулевой код выхода в OS.
Программа должна проходить проверки go vet и golint.
*/

func main() {
	// Выполняется запрос
	rBody, err := ntp.Query("0.ru.pool.ntp.org")

	// Ошибка запроса пишется в stderr, программа завершается с кодом 1
	if err != nil {
		fmt.Fprint(os.Stderr, err.Error())
		os.Exit(1)
	}

	// Проверка валидности тела ответа
	if err = rBody.Validate(); err != nil {
		fmt.Fprintf(os.Stderr, err.Error())
		os.Exit(2)
	}

	// Выводится текущее время + смещение
	fmt.Println(time.Now().Add(rBody.ClockOffset))
}
