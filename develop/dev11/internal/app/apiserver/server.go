package apiserver

import (
	"app/internal/app/config"
	"app/internal/app/handler"
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

func NewServer(cfg *config.Cfg) *http.Server {
	handler := handler.NewHandler()
	createRoutes(handler)

	return &http.Server{
		Addr:           cfg.GetAddr(),
		Handler:        handler.Router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		IdleTimeout:    30 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
}

func createRoutes(h *handler.Handler) {
	router := http.NewServeMux()

	router.HandleFunc("/create_event", h.Create)
	router.HandleFunc("/update_event", h.Update)
	router.HandleFunc("/delete_event", h.Delete)
	router.HandleFunc("/events_for_day", h.EventsForPeriod)
	router.HandleFunc("/events_for_week", h.EventsForPeriod)
	router.HandleFunc("/events_for_month", h.EventsForPeriod)

	h.Router = mwLogger(router)
}

func mwLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.String() == "/favicon.ico" {
			return
		}
		code := http.StatusNotFound
		ctx := context.WithValue(context.Background(), "status", &code)
		reqStart := time.Now()
		next.ServeHTTP(w, r.WithContext(ctx))
		duration := time.Since(reqStart)
		log.Printf("| %s | %-15s | %s | %v\n",
			colorize(code),
			duration,
			colorize(r.Method),
			r.URL,
		)
	})
}

func colorize(val interface{}) string {
	if v, ok := val.(int); ok {
		v = v / 100 * 100
		s := strconv.Itoa(v)
		return fmt.Sprintf("%s %v %s", colors[s], val, colors["Reset"])
	} else if v, ok := val.(string); ok {
		//blockSize := 7
		//s := v + strings.Repeat(" ", (blockSize-len(v)))
		return fmt.Sprintf("%s %-7s %s", colors[v], v, colors["Reset"])
	} else {
		return ""
	}
}

var colors = map[string]string{
	"200":   "\033[01;38;05;15;48;05;34m",
	"400":   "\033[01;38;05;15;48;05;196m",
	"500":   "\033[01;38;05;15;48;05;178m",
	"GET":   "\033[01;38;05;15;48;05;68m",
	"POST":  "\033[01;38;05;15;48;05;38m",
	"Reset": "\033[0m",
}
