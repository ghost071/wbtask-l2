package service

import (
	"app/internal/app/store"
	"app/internal/apperr"
	"app/internal/model"
	"fmt"
	"net/url"
	"strconv"
	"time"
)

const (
	ShortFormat = "01.02.2006"
	LongFormat  = "01.02.2006-15:04"
	// Parameters
	Date        = "date"
	TimeBegin   = "begin"
	Duration    = "duration"
	Description = "description"
	Title       = "title"
	ID          = "id"
)

func CreateEvent(storage *store.Cache, query string) (*model.Event, error) {
	vals, err := parseQuery(query)
	if err != nil {
		return nil, err
	}

	if err := checkExistance(vals, TimeBegin, Title); err != nil {
		return nil, err
	}

	startAt, err := parseTime(vals.Get(TimeBegin), LongFormat, ShortFormat)

	if err != nil {
		return nil, err
	}

	var duration time.Duration
	if vals.Has(Duration) {
		d, err := time.ParseDuration(vals.Get(Duration))
		if err != nil {
			return nil, apperr.NewValidateErr(err.Error())
		}
		duration = d
	}

	title, description := vals.Get(Title), vals.Get(Description)
	event := model.NewEvent(title, description, startAt, duration)
	key := startAt.Format(ShortFormat)
	storage.AddEvent(key, event)
	return event, nil
}

func DeleteEvent(storage *store.Cache, query string) (*model.Event, error) {
	vals, err := parseQuery(query)
	if err != nil {
		return nil, err
	}

	if err := checkExistance(vals, Date, ID); err != nil {
		return nil, err
	}

	_, err = parseTime(vals.Get(Date), ShortFormat)
	if err != nil {
		return nil, err
	}

	id, err := parseId(vals.Get(ID))
	if err != nil {
		return nil, err
	}

	event, err := storage.DeleteEvent(vals.Get(Date), int64(id))
	return event, err
}

func UpdateEvent(storage *store.Cache, query string) (*model.Event, error) {
	vals, err := parseQuery(query)
	if err != nil {
		return nil, err
	}

	if err := checkExistance(vals, Date, ID); err != nil {
		return nil, err
	}

	_, err = parseTime(vals.Get(Date), ShortFormat)
	if err != nil {
		return nil, err
	}

	id, err := parseId(vals.Get(ID))
	if err != nil {
		return nil, err
	}

	key := vals.Get(Date)
	event, err := storage.GetById(key, int64(id))
	if err != nil {
		return nil, err
	}

	newKey := key

	for key := range vals {
		val := vals.Get(key)
		if key == Date || key == ID {
			continue
		}

		switch key {
		case TimeBegin:
			startAt, err := parseTime(val, LongFormat, ShortFormat)
			if err != nil {
				return nil, err
			}
			newKey = startAt.Format(ShortFormat)
			event.SetTime(startAt)
		case Duration:
			d, err := time.ParseDuration(val)
			if err != nil {
				return nil, err
			}
			event.SetDuration(d)
		case Title:
			event.SetTitle(val)
		case Description:
			event.SetDescription(val)
		}
	}

	if key != newKey {
		storage.Move(event, key, newKey)
	}

	return event, nil
}

func EventsForPeriod(storage *store.Cache, query string, period int) ([]*model.Event, error) {
	vals, err := parseQuery(query)
	if err != nil {
		return nil, err
	}

	t, err := parseTime(vals.Get(Date), ShortFormat)
	if err != nil {
		return nil, err
	}

	res := make([]*model.Event, 0)

	for i := 0; i < period; i++ {
		events, _ := storage.GetEvents(t.Format(ShortFormat))
		res = append(res, events...)
		t = t.Add(time.Hour * 24)
	}

	if len(res) == 0 {
		return nil, apperr.NewNoContentErr("No events")
	}

	return res, nil
}

func checkExistance(v url.Values, keys ...string) error {
	for _, key := range keys {
		if !v.Has(key) {
			err := fmt.Errorf("parameter %s not found", key)
			return apperr.NewValidateErr(err.Error())
		}
	}
	return nil
}

func parseQuery(query string) (v url.Values, err error) {
	v, err = url.ParseQuery(query)
	if err != nil {
		err = apperr.NewValidateErr(err.Error())
	}
	return v, err
}

func parseTime(val string, formats ...string) (t time.Time, err error) {
	for _, f := range formats {
		t, err = time.Parse(f, val)
		if err == nil {
			return t, err
		}
	}
	err = apperr.NewValidateErr(err.Error())
	return t, err
}

func parseId(s string) (int, error) {
	n, err := strconv.Atoi(s)
	if err != nil {
		return n, apperr.NewValidateErr(err.Error())
	}
	return n, nil
}
