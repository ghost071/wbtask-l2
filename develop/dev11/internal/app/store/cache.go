package store

import (
	"app/internal/apperr"
	"app/internal/model"
	"sync"
)

type Cache struct {
	rwm    *sync.RWMutex
	events map[string][]*model.Event
}

func NewCache() *Cache {
	return &Cache{
		rwm:    &sync.RWMutex{},
		events: make(map[string][]*model.Event),
	}
}

func (c *Cache) GetEvents(key string) ([]*model.Event, error) {
	c.rwm.RLock()
	v, ok := c.events[key]
	c.rwm.RUnlock()

	if !ok {
		return nil, apperr.NewNoContentErr("No Content")
	}
	return v, nil
}

func (c *Cache) AddEvent(key string, event *model.Event) {
	c.rwm.Lock()
	event.SetIndex(len(c.events[key]))
	c.events[key] = append(c.events[key], event)
	c.rwm.Unlock()
}

func (c *Cache) DeleteEvent(key string, id int64) (*model.Event, error) {
	event, err := c.GetById(key, id)
	if err != nil {
		return event, err
	}

	c.rwm.Lock()
	idx := event.GetIndex()
	c.events[key] = append(c.events[key][:idx], c.events[key][idx+1:]...)
	c.rwm.Unlock()
	return event, nil
}

func (c *Cache) GetById(key string, id int64) (event *model.Event, err error) {
	events, err := c.GetEvents(key)
	if err != nil {
		return nil, err
	}

	for _, e := range events {
		c.rwm.RLock()
		if e.ID == id {
			event = e
		}
		c.rwm.RUnlock()
	}
	if event == nil {
		return event, apperr.NewNoContentErr("No Content")
	}
	return event, nil
}

func (c *Cache) UpdateEvent(key string, id int64, updated *model.Event) (*model.Event, error) {
	event, err := c.GetById(key, id)
	if err != nil {
		return event, err
	}
	idx := event.GetIndex()
	updated.SetIndex(idx)

	event = updated

	return event, nil
}

func (c *Cache) Move(event *model.Event, src, dst string) {
	c.DeleteEvent(src, event.ID)
	c.AddEvent(dst, event)
}
