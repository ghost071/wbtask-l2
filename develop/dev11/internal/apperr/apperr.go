package apperr

import "fmt"

// TODO wrap/unwrap methods

type ServiceError struct {
	Msg string
}

func (v *ServiceError) Error() string {
	return v.Msg
}

type ValidateError struct {
	Msg string
}

func (v *ValidateError) Error() string {
	return v.Msg
}

type NoContentError struct {
	Msg string
}

func (n *NoContentError) Error() string {
	return n.Msg
}

func NewServiceErr(s string) *ServiceError {
	return &ServiceError{
		Msg: fmt.Sprintf("[SERVICE]: %s\n", s),
	}
}

func NewValidateErr(s string) *ValidateError {
	return &ValidateError{
		Msg: fmt.Sprintf("[VALIDATOR]: %s\n", s),
	}
}

func NewNoContentErr(s string) *NoContentError {
	return &NoContentError{
		Msg: s,
	}
}

type MethodNotAllowedError struct {
	Msg string
}

func (e *MethodNotAllowedError) Error() string {
	return e.Msg
}

func NewNotAllowedErr(s string) *MethodNotAllowedError {
	return &MethodNotAllowedError{
		Msg: s,
	}
}
