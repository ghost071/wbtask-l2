package main

import (
	"bufio"
	"errors"
	"flag"
	"io"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"unicode"
)

/*
=== Утилита sort ===

Отсортировать строки (man sort)
Основное

Поддержать ключи

-k — указание колонки для сортировки
-n — сортировать по числовому значению
-r — сортировать в обратном порядке
-u — не выводить повторяющиеся строки

Дополнительное

Поддержать ключи

-M — сортировать по названию месяца
-b — игнорировать хвостовые пробелы
-c — проверять отсортированы ли данные
-h — сортировать по числовому значению с учётом суффиксов

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type Options struct {
	flagK int
	flagN bool
	flagU bool
	flagR bool
}

//type lessFunc func(s1, s2 string, col int) bool

type SortStrings struct {
	data []string
	opt  *Options
}

func (s *SortStrings) Swap(i, j int) {
	s.data[i], s.data[j] = s.data[j], s.data[i]
}

func (s *SortStrings) Len() int {
	return len(s.data)
}

func (s *SortStrings) Less(i, j int) bool {
	s1, s2 := s.data[i], s.data[j]
	opt := s.opt

	byNum := opt.flagN
	desc := opt.flagR
	col := opt.flagK - 1

	fields1, fields2 := strings.Fields(s1), strings.Fields(s2)

	switch {
	case byNum:
		return lessByNum(fields1, fields2, col) != desc
	default:
		return lessByWords(fields1, fields2, col) != desc
	}
}

func (s *SortStrings) Sort() {
	sort.Sort(s)
}

func lessByWords(s1, s2 []string, col int) bool {
	l1, l2 := len(s1), len(s2)

	w1, w2 := cut(s1, col, l1), cut(s2, col, l2)

	if w1 != w2 {
		return w1 < w2
	}

	w1, w2 = cut(s1, 0, col), cut(s2, 0, col)

	return w1 < w2
}

func cut(s []string, begin, end int) string {
	if begin > len(s) {
		return ""
	}
	return strings.Join(s[begin:end], "")
}

func lessByNum(s1, s2 []string, col int) bool {
	l1, l2 := len(s1), len(s2)

	w1, w2 := cut(s1, col, l1), cut(s2, col, l2)
	n1, n2 := getNum(w1), getNum(w2)

	if n1 != n2 {
		return n1 < n2
	}
	return lessByWords(s1, s2, 0)
}

func getNum(s string) int {
	sNum := ""

	for _, n := range []rune(s) {
		if !unicode.IsDigit(n) {
			break
		}
		sNum += string(n)
	}

	n, _ := strconv.Atoi(sNum)
	return n
}

func main() {
	opt := new(Options)
	flag.IntVar(&opt.flagK, "k", 1, "sort strings by column")
	flag.BoolVar(&opt.flagN, "n", false, "sort strings by arithmetic value")
	flag.BoolVar(&opt.flagR, "r", false, "sort in desc order")
	flag.BoolVar(&opt.flagU, "u", false, "return only unique strings")

	flag.Parse()

	var data []string

	filename := flag.Arg(0)

	switch filename {
	case "":
		if err := readFile(os.Stdin, &data); err != nil {
			log.Fatal(err)
		}
	default:
		f, err := os.Open(filename)

		if err != nil {
			log.Fatal(err)
		}

		if err = readFile(f, &data); err != nil {
			log.Fatal(err)
		}
	}
	sortS(&data, opt)
}

func readFile(f io.Reader, buffer *[]string) error {
	r := bufio.NewReader(f)
	for {
		str, err := r.ReadString('\n')
		strings.Trim(str, "\n")
		if err == io.EOF {
			break
		}

		if err != nil {
			return err
		}
		(*buffer) = append((*buffer), str)
	}
	return nil
}

func sortS(data *[]string, opt *Options) {
	sortObj := &SortStrings{
		data: (*data),
		opt:  opt,
	}

	if opt.flagK < 1 {
		log.Fatal(errors.New("k - must be >= 1"))
	}

	sortObj.Sort()
	err := write(os.Stdout, data, opt.flagU)

	if err != nil {
		log.Println(err)
	}
}

func write(f *os.File, data *[]string, flag bool) error {
	for i, str := range *data {
		if i > 0 && flag && str == (*data)[i-1] {
			continue
		}

		_, err := io.WriteString(f, str)
		if err != nil {
			return err
		}

	}
	return nil
}
