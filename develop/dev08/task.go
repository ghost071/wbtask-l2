package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

/*
=== Взаимодействие с ОС ===

Необходимо реализовать собственный шелл

встроенные команды: cd/pwd/echo/kill/ps
поддержать fork/exec команды
конвеер на пайпах

Реализовать утилиту netcat (nc) клиент
принимать данные из stdin и отправлять в соединение (tcp/udp)
Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type shell struct {
	handler *handler
}

type handler struct{}

func (s *shell) executeCmd(commands []string, stdout string) error {

	args := parseArgs(commands[0])

	switch args[0] {
	case "cd":
		if err := s.handler.ChDir(args[1:]...); err != nil {
			return err
		}
		return nil

	case "kill":
		if err := s.handler.KillPs(args[1:]...); err != nil {
			return err
		}
		return nil

	case "pwd":
		newStdout, err := os.Getwd()
		if err != nil {
			return err
		}

		if len(commands) > 1 {
			return s.executeCmd(commands[1:], newStdout)
		}

		fmt.Println(newStdout)
		return nil

	case "echo":
		newStdout := strings.Join(args[1:], " ")
		if len(commands) > 1 {
			return s.executeCmd(commands[1:], newStdout)
		}
		fmt.Println(newStdout)
		return nil

	case `\quit`:
		os.Exit(0)
	default:

		cmd := exec.Command(args[0], args[1:]...)

		stdin, err := cmd.StdinPipe()
		if err != nil {
			return err
		}

		go func() {
			defer stdin.Close()
			io.WriteString(stdin, stdout)
		}()

		if len(commands) == 1 {
			cmd.Stdout = os.Stdout
			if err := cmd.Start(); err != nil {
				return err
			}
			cmd.Wait()
			return nil
		}

		newStdout, err := cmd.StdoutPipe()

		if err != nil {
			return err
		}

		if err = cmd.Start(); err != nil {
			return err
		}
		data, _ := io.ReadAll(newStdout)
		newStdout.Close()

		return s.executeCmd(commands[1:], string(data))
	}
	return nil
}

const (
	redWithUnderLine string = "\033[01;04;38;05;196m"
	red                     = "\033[01;38;05;196m"
	arrow                   = "\u1405"
	greeting                = "========= WELCOME TO THE SHELL =========="
	reset                   = "\033[0m"
)

func main() {
	shell := newShell()
	shell.Run()
}

func newShell() *shell {
	return &shell{&handler{}}
}

func (s *shell) Run() {
	fmt.Printf("%s%s%s\n", redWithUnderLine, greeting, reset)
	scanner := bufio.NewScanner(os.Stdout)
	for {
		fmt.Printf("%s%s%s ", red, arrow, reset)
		scanner.Scan()
		commands := parseCmd(scanner.Text())

		if err := s.executeCmd(commands, ""); err != nil {
			fmt.Println(err)
		}
	}
}

func parseCmd(cmd string) []string {
	commands := strings.Split(cmd, "|")
	for i, _ := range commands {
		commands[i] = strings.TrimLeft(commands[i], " ")
		commands[i] = strings.TrimRight(commands[i], " ")
	}
	return commands
}

func parseArgs(cmd string) []string {
	args := make([]string, 0)
	quote := false
	curr := ""

	for _, r := range []rune(cmd) {
		switch {
		case string(r) == " " && !quote:
			args = append(args, curr)
			curr = ""
		case string(r) == `"` && !quote:
			quote = true
		case string(r) == `"` && quote:
			quote = false
			args = append(args, curr)
			curr = ""
		default:
			curr += string(r)
		}
	}
	if curr != "" {
		args = append(args, curr)
	}
	return args
}

func (h *handler) ChDir(args ...string) error {
	if len(args) == 0 {
		return os.Chdir(os.Getenv("HOME"))
	}
	if len(args) > 1 {
		return errors.New("cd: too many args")
	}

	return os.Chdir(args[0])
}

func (h *handler) KillPs(args ...string) error {
	if len(args) < 1 {
		return errors.New("kill: need process pid")
	}

	pid, err := strconv.Atoi(args[0])

	if err != nil {
		return err
	}

	proc, err := os.FindProcess(pid)
	if err != nil {
		return err
	}
	if err = proc.Kill(); err != nil {
		return err
	}

	proc.Wait()
	return nil
}
