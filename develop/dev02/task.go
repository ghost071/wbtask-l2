package main

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"unicode"
)

/*
=== Задача на распаковку ===

Создать Go функцию, осуществляющую примитивную распаковку строки, содержащую повторяющиеся символы / руны, например:
	- "a4bc2d5e" => "aaaabccddddde"
	- "abcd" => "abcd"
	- "45" => "" (некорректная строка)
	- "" => ""
Дополнительное задание: поддержка escape - последовательностей
	- qwe\4\5 => qwe45 (*)
	- qwe\45 => qwe44444 (*)
	- qwe\\5 => qwe\\\\\ (*)

В случае если была передана некорректная строка функция должна возвращать ошибку. Написать unit-тесты.

Функция должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func unzipString(s string) (string, error) {
	runes := []rune(s)
	res := make([]string, 0)

	i := 0
	tmp := ""

	for i < len(runes) {
		switch {
		case unicode.IsDigit(runes[i]):
			return "", errors.New("Invalid string: can't begin with digit")
		case string(runes[i]) == `\`:
			if i == len(runes)-1 {
				return "", errors.New("Invalid escape seq")
			}
			tmp += string(runes[i+1])
			i += 2
			repeat(&tmp, &i, &runes)
			res = append(res, tmp)
			tmp = ""
		default:
			tmp += string(runes[i])
			i += 1
			repeat(&tmp, &i, &runes)
			res = append(res, tmp)
			tmp = ""
		}
	}
	return strings.Join(res, ""), nil
}

func repeat(s *string, n *int, runes *[]rune) {
	num := ""
	for *n < len(*runes) && unicode.IsDigit((*runes)[*n]) {
		num += string((*runes)[*n])
		*n = *n + 1
	}

	if num == "" {
		return
	}

	x, err := strconv.Atoi(num)

	if err != nil {
		log.Fatal(err)
	}
	*s = strings.Repeat(*s, x)
}

func main() {
	example := `🍺5m3odµ2\32`
	s, _ := unzipString(example)
	fmt.Printf("%s --> %s\n", example, s)
}
