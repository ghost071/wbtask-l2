package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnzipStrign(t *testing.T) {
	type result struct {
		res string
		err error
	}

	cases := map[string]result{
		`a4bc2d5e`: result{`aaaabccddddde`, nil},
		`abcd`:     result{`abcd`, nil},
		`45`:       result{"", errors.New("Invalid string: can't begin with digit")},
		``:         result{"", nil},
		`д3ж13фф`:  result{"ддджжжжжжжжжжжжжфф", nil},
		`g\110\\2`: result{`g1111111111\\`, nil},
		`\`:        result{"", errors.New("Invalid escape seq")},
	}

	for key, ans := range cases {
		str, err := unzipString(key)

		assert.Equal(t, str, ans.res, "Wrong result")

		switch {
		case err != nil && ans.err == nil:
			assert.Nil(t, err, err.Error())
		case err == nil && ans.err != nil:
			assert.NotNil(t, err, "Must be error")
		case err != nil:
			assert.EqualError(t, err, ans.err.Error())
		}
	}
}
